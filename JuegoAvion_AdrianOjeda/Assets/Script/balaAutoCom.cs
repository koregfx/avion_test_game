﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class balaAutoCom : MonoBehaviour 
{
	public float velocidadbala;
	private GameObject player;
	public float VelocidadRot;
	public GameObject particles;
	
	void Start () 
	{
		Destroy(this.gameObject, 10f);
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	void Update () 
	{
		movimientoBala();
	}
	void OnCollisionEnter(Collision collision)
    {
			GameObject particulas = Instantiate(particles, transform.position, Quaternion.identity );
		Destroy(particulas, 1f);
		Destroy(this.gameObject);
		
	}
	private void movimientoBala()
	{

		transform.Translate(transform.forward * velocidadbala * Time.deltaTime,Space.World);

		this.transform.LookAt(player.transform);

	}
}
