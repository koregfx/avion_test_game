﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemyRafagaUi : MonoBehaviour {

public GameObject torreta;
public GameObject camara;
public Text vida;
public Image life;
public float vidaNum;
	void Start () 
	{
		camara = GameObject.FindGameObjectWithTag("camara");
	}
	
	void Update () 
	{
		transform.LookAt(camara.transform);
		vida.text = torreta.GetComponent<ControladorTorretaRafaga>().vidaTorre.ToString("00");
		vidaNum =  torreta.GetComponent<ControladorTorretaRafaga>().vidaTorre/10;
		life.fillAmount = vidaNum;
		if(torreta.GetComponent<ControladorTorretaRafaga>().vidaTorre < 5)
		{
			life.color = Color.yellow;
		}
		if(torreta.GetComponent<ControladorTorretaRafaga>().vidaTorre < 2)
		{
			life.color = Color.red;
		}
	}
}
