﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorTorretaAuto : MonoBehaviour
{

	private GameObject player;

	public GameObject CabezaTorreta;
	public GameObject CanonTorreta;
	public GameObject salidaCanon;

	public float VelocidadRotacion;

	public float tiempoCadencia;
	private float tiempoAcumulado;
	public GameObject bala;
	private bool estadisparando;
	public float velocidadBala;
	public float torretaAvion;
	public float rangoVisionMax;
	public float rangoDisparoMax;
	public float vidaTorre;
	public GameObject particulas;

	void Start ()
	{
		player = GameObject.FindGameObjectWithTag("Player");	
	
	}
	

	void Update () 
	{
		//RANGOS DE VISION DE LA TORRETA
		torretaAvion = Vector3.Distance(player.transform.position, this.transform.position);
		if(torretaAvion < rangoVisionMax)
		{
			ApuntarTarget();
		}
		

		if(torretaAvion < rangoDisparoMax)
		{
			if(!estadisparando)
			{
				DisparosTorreta();
			}
		}
		if(vidaTorre <= 0)
		{
			GameObject explosion = Instantiate(particulas, salidaCanon.transform.position, Quaternion.identity);
			Destroy(explosion,3f);
			manager.Mannager.GetComponent<manager>().cuentaAuto ++;
			Destroy(this.gameObject);
			
		}
		
	}

	private void ApuntarTarget()
	{
		// APUNTAR EL CAÑON A EL AVION
		Vector3 direccion = player.transform.position - CabezaTorreta.transform.position;
		Vector3 apuntarTorre = new Vector3(direccion.x, 0, direccion.z);
		Quaternion rotacion = Quaternion.LookRotation(apuntarTorre.normalized, Vector3.up);
		CabezaTorreta.transform.rotation = Quaternion.Slerp(CabezaTorreta.transform.rotation, rotacion, VelocidadRotacion * Time.deltaTime);

		CanonTorreta.transform.LookAt(player.transform);
	}

	private void DisparosTorreta()
	{
		// DISPARO DE LA TORRETA
		Instantiate(bala, salidaCanon.transform.position, Quaternion.identity );
		
		StartCoroutine(recargaTorreta());
	}

	void OnCollisionEnter(Collision collision)
 	{
		if(collision.gameObject.tag == "playermun")
		{
			vidaTorre --;
		}
	}
	IEnumerator recargaTorreta()
	{
		estadisparando = true;
		yield return new WaitForSeconds (tiempoCadencia);

		estadisparando = false;
		yield return 0;
	}
	
}
