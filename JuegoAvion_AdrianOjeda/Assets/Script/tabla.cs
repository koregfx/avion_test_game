﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tabla : MonoBehaviour 
{
	public Text partidaPunt;
	public Text primeraPunt;
	public Text segundaPunt;
	public Text terceraPunt;

	float primeraPuntuacion;
	float segundaPuntuacion;
	float terceraPuntuacion;
	float puntuacionActual;
	string primerNombre;
	string segundoNombre;
	string nombreActual;

	void Start () 
	{
		puntuacionActual = PlayerPrefs.GetFloat("TiempoPartida");
		primeraPuntuacion = PlayerPrefs.GetFloat("PrimerTiempo");
		segundaPuntuacion = PlayerPrefs.GetFloat("SegundoTiempo");
		terceraPuntuacion = PlayerPrefs.GetFloat("TercerTiempo");

		nombreActual = PlayerPrefs.GetString("NombreActual");
		primerNombre = PlayerPrefs.GetString("PrimerNombre");
		segundoNombre = PlayerPrefs.GetString("SegundoNombre");


		if(puntuacionActual < primeraPuntuacion)
		{
			PlayerPrefs.SetFloat("TercerTiempo", segundaPuntuacion);
			PlayerPrefs.SetFloat("SegundoTiempo", primeraPuntuacion);
			PlayerPrefs.SetFloat("PrimerTiempo", puntuacionActual);
			PlayerPrefs.SetString("TercerNombre", segundoNombre);
			PlayerPrefs.SetString("SegundoNombre", primerNombre);
			PlayerPrefs.SetString("PrimerNombre", nombreActual);
		}
		if(puntuacionActual > primeraPuntuacion && puntuacionActual < segundaPuntuacion)
		{
			PlayerPrefs.SetFloat("TercerTiempo", segundaPuntuacion);
			PlayerPrefs.SetFloat("SegundoTiempo", puntuacionActual);
			PlayerPrefs.SetString("TercerNombre", segundoNombre);
			PlayerPrefs.SetString("SegundoNombre", nombreActual);
		}
		if(puntuacionActual > segundaPuntuacion && puntuacionActual < terceraPuntuacion)
		{
			PlayerPrefs.SetFloat("TercerTiempo", puntuacionActual);
			PlayerPrefs.SetString("TercerNombre", nombreActual);
		}

		partidaPunt.text = PlayerPrefs.GetString("NombreActual") + "   " + PlayerPrefs.GetFloat("TiempoPartida").ToString("0000000");
		primeraPunt.text = PlayerPrefs.GetString("PrimerNombre") + "   " + PlayerPrefs.GetFloat("PrimerTiempo").ToString("0000000");
		segundaPunt.text = PlayerPrefs.GetString("SegundoNombre") + "   " + PlayerPrefs.GetFloat("SegundoTiempo").ToString("0000000");
		terceraPunt.text = PlayerPrefs.GetString("TercerNombre") + "   " + PlayerPrefs.GetFloat("TercerTiempo").ToString("0000000");
	}
	
	
}
