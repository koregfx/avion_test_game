﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class balaRafaga : MonoBehaviour 
{
public GameObject flare;

	void Start () 
	{
		transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);
	
		Destroy(this.gameObject, 6f);
	}
	
	void Update () 
	{
	}
	
	 private void OnTriggerEnter(Collider other)
    {
		Destroy(this.gameObject);
	}
	void OnCollisionEnter(Collision collision)
  {
		GameObject particulas = Instantiate(flare, transform.position, Quaternion.identity );
		Destroy(particulas, 1f);
		Destroy(this.gameObject);
	}
}
