﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorTorretaRafaga : MonoBehaviour
{

	private GameObject player;

	public GameObject CabezaTorreta;
	public GameObject CanonTorreta;
	public GameObject salidaCanon;

	public float VelocidadRotacion;

	public float tiempoCadencia;
	private float tiempoAcumulado;
	public GameObject bala;
	private bool estadisparando;
	public float velocidadBala;
	private int[] tiempomisiles;
	private int misilActual;
	private float torretaAvion;
	public float rangoVision;
	public float rangodisparo;
	public GameObject[] misiltabla;
	public float rotacionY;
	public float vidaTorre;
	public GameObject particulas;




	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("Player");
		vidaTorre = 10;
		tiempomisiles = new int[4];
		
		tiempomisiles[0] = 1;
		tiempomisiles[1] = 1;
		tiempomisiles[2] = 1;
		tiempomisiles[3] = 9;
		misilActual = 0;
	}
	

	void Update () 
	{
		torretaAvion = Vector3.Distance(player.transform.position, this.transform.position);

		if(torretaAvion < rangoVision)
		{
			ApuntarTarget();
		}

		tiempoCadencia = tiempomisiles[misilActual];
		
		if(!estadisparando && torretaAvion < rangodisparo)
		{
			DisparosTorreta();
		}
		else
		{
			tiempoAcumulado += Time.deltaTime;
			if(tiempoAcumulado > tiempoCadencia)
			{
				estadisparando = false;
				tiempoAcumulado = 0;
				misilActual ++;
				if(misilActual > 3)
				{
					misilActual = 0;
				}
			}
		}
		if(vidaTorre <= 0)
		{
			GameObject explosion = Instantiate(particulas, salidaCanon.transform.position, Quaternion.identity);
			Destroy(explosion,3f);
			manager.Mannager.GetComponent<manager>().cuentaRafaga ++;
			Destroy(this.gameObject);
			
		}
		

	}

	private void ApuntarTarget()
	{
		Vector3 direccion = player.transform.position - CabezaTorreta.transform.position;
		Vector3 apuntarTorre = new Vector3(direccion.x, 0, direccion.z);
		Quaternion rotacion = Quaternion.LookRotation(apuntarTorre.normalized, Vector3.up);
		CabezaTorreta.transform.rotation = Quaternion.Slerp(CabezaTorreta.transform.rotation, rotacion, VelocidadRotacion * Time.deltaTime);
		
		CanonTorreta.transform.LookAt(player.transform);
		//LIMITES DE GIRO
		if(EulerSafex(CanonTorreta.transform.eulerAngles.x) > 10 && (CanonTorreta.transform.position.y - player.transform.position.y) > 0)
		{
			CanonTorreta.transform.eulerAngles = new Vector3 ( 10f, CanonTorreta.transform.eulerAngles.y, CanonTorreta.transform.eulerAngles.z);
		}
		if(EulerSafex(CanonTorreta.transform.eulerAngles.x) <  120 && (CanonTorreta.transform.position.y - player.transform.position.y) < 0)
		{
			CanonTorreta.transform.eulerAngles = new Vector3 ( -60f, CanonTorreta.transform.eulerAngles.y, CanonTorreta.transform.eulerAngles.z);
		}
	
	}

	private void DisparosTorreta()
	{
		GameObject proyectil = Instantiate(misiltabla[misilActual], salidaCanon.transform.position, Quaternion.identity);
		proyectil.GetComponent<Rigidbody>().AddForce(salidaCanon.transform.forward * velocidadBala , ForceMode.Impulse);
	
	
		StartCoroutine(recargaTorreta());
		
	}
	void OnCollisionEnter(Collision collision)
  {
		if(collision.gameObject.tag == "playermun")
		{
			vidaTorre --;
		}
	}
	public float EulerSafex(float x)
	{
		if(x > -90 && x <= 90)
		{
			return x;
		}
		if(x>0)
		{
			x -= 180;
		}
		else
		{
			x += 180;
		}

		return x;


	}

	IEnumerator recargaTorreta()
	{
		estadisparando = true;
		yield return new WaitForSeconds (tiempoCadencia);

		estadisparando = false;
		misilActual ++;
		if(misilActual > 3)
		{
			misilActual = 0;
		}
		yield return 0;
	}	
	
	
}
