﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorCamara : MonoBehaviour
{
	private GameObject player;

	private Vector3 offset;

	public float Damping;
	private bool isfree;
	public float velRot;
	public Vector3 movimiento;
	public Quaternion rotacion;

	void Start ()
	{
		isfree = true;
		Damping = 2;
		
		velRot = 10;
		player = GameObject.FindGameObjectWithTag("Player");		
		offset = player.transform.position - this.transform.position;
	}
	
	void Update ()
	{
		//SIGUE AL AVION PERO NO ROTA
		//this.transform.position = player.transform.position - offset;
		if(Input.GetKeyDown(KeyCode.Mouse0))
		{
			isfree = false;
		}
		if(Input.GetKeyUp(KeyCode.Mouse0))
		{
			isfree = true;
		}

		if(isfree)
		{
			ControlCamara();
		}
		else
		{
			CamaraLibre();
		}
	}

	private void ControlCamara()
	{
		//CONTROL CAMARA MOVIMIENTO Y ROTACION
		Vector3  anguloDeseado = player.transform.eulerAngles;
		rotacion = Quaternion.Euler(anguloDeseado);

		movimiento = player.transform.position - (rotacion * offset);
		this.transform.position = Vector3.Lerp (this.transform.position, movimiento, Damping * Time.deltaTime);

		this.transform.LookAt(player.transform);
	}
	private void CamaraLibre()
	{
		//LOGICA DE LA CAMARA LIBRE
		float rotY = velRot * Input.GetAxis("Mouse X");
        float rotX = velRot * Input.GetAxis("Mouse Y");
		
		transform.Rotate(-rotX, rotY, 0);
      	movimiento = player.transform.position -  offset;
		this.transform.position = Vector3.Lerp (this.transform.position, movimiento, Damping * Time.deltaTime);
	}
}
