﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiPlayer : MonoBehaviour 
{

public GameObject camara;
public Text vida;
public Image life;
public Image munition;
public float vidaNum;
public float munNum;
	void Start () 
	{
		munition.color = Color.green;
	}
	
	void Update () 
	{
		transform.LookAt(camara.transform);
		vida.text = manager.Mannager.player.GetComponent<ControladorAvion>().vida.ToString("00");
		vidaNum =  manager.Mannager.player.GetComponent<ControladorAvion>().vida/10;
		life.fillAmount = vidaNum;
		if(manager.Mannager.player.GetComponent<ControladorAvion>().vida < 5)
		{
			life.color = Color.yellow;
		}
		if(manager.Mannager.player.GetComponent<ControladorAvion>().vida < 2)
		{
			life.color = Color.red;
		}
		munNum =  manager.Mannager.player.GetComponent<ControladorAvion>().municion/100;
		munition.fillAmount = munNum;
		if(manager.Mannager.player.GetComponent<ControladorAvion>().municion > 40)
		{
			munition.color = Color.green;
		}
		if(manager.Mannager.player.GetComponent<ControladorAvion>().municion < 40)
		{
			munition.color = Color.yellow;
		}
		if(manager.Mannager.player.GetComponent<ControladorAvion>().municion < 10)
		{
			munition.color = Color.red;
		}
	}
}
