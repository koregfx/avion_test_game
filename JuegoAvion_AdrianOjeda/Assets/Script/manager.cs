﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class manager : MonoBehaviour 
{
	private static manager mannager;
	public static manager Mannager
	{
		get
		{
			if(mannager == null)
			{
				mannager = GameObject.Find("GameMannager").GetComponent<manager>();
			}
			return mannager;
		}
	}
	public GameObject cajaMunicion;
	public GameObject player;
	public bool spawnCaja;
	public float radioSpawnCaja;
	public bool randomSpanCaja;
	public int[] spawnNivel1;
	public int[] spawnNivel2;
	public int[] spawnNivel3;
	public int[] spawnNivel4;
	public int[] spawnNivel5;
	public int[] nivelActual;
	public int cuentaRafaga;
	public int cuentaAuto;
	private bool spawnRafaga;
	private bool spawnAuto;
	public GameObject torreAuto;
	public GameObject torreRafaga;
	public float radioSpawntorre;
	public int nivelEnter;
	public GameObject[]  posiciones;
	List<int> posicionesCogidas = new List<int>();
	public bool viablePoint; 
	public bool enJuego;
	public bool finNivel;	
	public float timeSec;
	private float timeFinal;
	public string nombre;
	public InputField Entrada;

	void Start () 
	{
		viablePoint = true;
		player = GameObject.FindGameObjectWithTag("Player");
		posiciones = GameObject.FindGameObjectsWithTag("posicion");
		spawnRafaga = true;
		spawnAuto = true;
		nivelActual = spawnNivel1;
		nivelEnter = 0;
		cuentaAuto = 0;
		cuentaRafaga = 0;
		enJuego = true;
		StartCoroutine(cuentaTiempo());
		nombre = PlayerPrefs.GetString("NombreActual");
		timeSec = 0;
	}

	void Update () 
	{
		
		if(player.GetComponent<ControladorAvion>().municion < 30 && spawnCaja == false)
		{
			spawnMunicion();
		}
		if(Random.value > 0.999f && randomSpanCaja== false)
		{
			spawnMunicionRand();
		}
		if(spawnRafaga == true)
		{
			spawnTorreRafaga();
		}
		if(spawnAuto == true)
		{
			spawnTorreAuto();
		}

		if (cuentaRafaga == nivelActual[0] && cuentaAuto == nivelActual[1])
		{
			nivelEnter ++;
			cuentaAuto = 0;
			cuentaRafaga = 0;
			spawnAuto = true;
			spawnRafaga = true;
			posicionesCogidas = null;
		}
		if(nivelEnter == 1)
		{
			nivelActual = spawnNivel2;
		}
		if(nivelEnter == 2)
		{
			nivelActual = spawnNivel3;
		}
		if(nivelEnter == 3)
		{
			nivelActual = spawnNivel4;
		}
		if(nivelEnter == 4)
		{
			nivelActual = spawnNivel5;
		}
		if(nivelEnter == 5)
		{
			timeFinal = timeSec;
			PlayerPrefs.SetFloat("TiempoPartida", timeFinal);
			StartCoroutine(victoria());
		}

		if(player.GetComponent<ControladorAvion>().vida <= 0)
		{
			timeFinal = timeSec;
			PlayerPrefs.SetFloat("TiempoPartida", 999999999);
			derrota();
		}
	}

	private void spawnMunicion()
	{
		Vector3 spawnPsoition;
		spawnPsoition = player.transform.position + (Random.insideUnitSphere * radioSpawnCaja);
		spawnCaja = true;
		Instantiate(cajaMunicion, spawnPsoition, Quaternion.identity );

	}
	private void spawnMunicionRand()
	{
		
		Vector3 spawnPsoition;
		spawnPsoition = player.transform.position + (Random.insideUnitSphere * radioSpawnCaja);
		if(spawnPsoition.y <= 0)
		{
			spawnPsoition.y = 2;
		}
		randomSpanCaja = true;
		Instantiate(cajaMunicion, spawnPsoition, Quaternion.identity );

	}
	private void spawnTorreRafaga()
	{
		for(int i = 0; i < nivelActual[0]; i++)
		{
			int x;
			x = Mathf.RoundToInt(Random.Range(1,6));

			Vector3 spawnPsoition;
			spawnPsoition = posiciones[x].transform.position;
			Instantiate(torreRafaga, spawnPsoition, Quaternion.identity );
			
			
		}
		spawnRafaga = false;
	}
	private void spawnTorreAuto()
	{
		for(int i = 0; i <nivelActual[1]; i++)
		{
			while(!viablePoint)
			{
				int x;
				x = Mathf.RoundToInt(Random.Range(1,6));
				for(int o = 0; o <posicionesCogidas.Count; o++)
				{
					if(x != posicionesCogidas[o])
					{
						viablePoint = true;
					}
					else
					{
						viablePoint = false;
					}
				}
			}
			Vector3 spawnPsoition;
			spawnPsoition = posiciones[Mathf.RoundToInt(Random.Range(1,6))].transform.position;
			Instantiate(torreAuto, spawnPsoition, Quaternion.identity );			
		}
		spawnAuto = false;
	}
	/*
	private void spawnTorreRafaga()
	{
		for(int i = 0; i < nivelActual[0]; i++)
		{
			int x;
			x = Mathf.RoundToInt(Random.Range(1,6));

			while(!viablePoint)
			{
				
				x = Mathf.RoundToInt(Random.Range(1,6));

				for(int o = 0; o <posicionesCogidas.Count; o++)
				{
					if(posicionesCogidas.Contains(x))
					{
						viablePoint = false;
					}
				}
			}
			Vector3 spawnPsoition;
			spawnPsoition = posiciones[Mathf.RoundToInt(Random.Range(1,6))].transform.position;
			Instantiate(torreRafaga, spawnPsoition, Quaternion.identity );		
			posicionesCogidas.Add(x);
		}
		spawnRafaga = false;
	}
	private void spawnTorreAuto()
	{
		for(int i = 0; i <nivelActual[1]; i++)
		{
			int x;
			x = Mathf.RoundToInt(Random.Range(1,6));
			while(!viablePoint)
			{
				
				x = Mathf.RoundToInt(Random.Range(1,6));

				for(int o = 0; o <posicionesCogidas.Count; o++)
				{
					if(posicionesCogidas.Contains(x))
					{
						viablePoint = false;
					}
				}
			}
			Vector3 spawnPsoition;
			spawnPsoition = posiciones[Mathf.RoundToInt(Random.Range(1,6))].transform.position;
			Instantiate(torreAuto, spawnPsoition, Quaternion.identity );		
			posicionesCogidas.Add(x);	
		}
		spawnAuto = false;
	}
	*/


	public void cambioEscena(int i)
	{
		SceneManager.LoadScene(i,LoadSceneMode.Single);
	}

	public void Quit()
	{
		Application.Quit();
	}
	public void derrota()
	{
	
		StartCoroutine(timingderrota());
	}

	IEnumerator victoria()
	{
		yield return new WaitForSeconds(1f);

		cambioEscena(2);
		yield return 0; 
	}
	IEnumerator timingderrota()
	{
		
		yield return new WaitForSeconds(1f);

		cambioEscena(3);
		yield return 0; 
	}
	IEnumerator cuentaTiempo()
	{
		while(enJuego)
		{
			if(!finNivel)
			{
				timeSec += 1;
			}		
			
			yield return new WaitForSeconds (1f);
		}
		
	}
	public void guardarNombre()
	{
		PlayerPrefs.SetString("NombreActual", Entrada.text);
	}
	
}
