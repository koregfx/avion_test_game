﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorAvion : MonoBehaviour 
{
	public float velocidadMov;
	public float velocidadGiro;
	public float velMax;
	public float velMin;
	public float aceleracionMov;
	public GameObject elice;
	public GameObject bala;
	public float velocidadBala;
	public GameObject salidaCanon1;
	public GameObject salidaCanon2;
	private bool estadisparando;
	private bool changeTorreta;
	private float tiempoAcumulado;
	public float tiempoCadencia;
	private bool repararCadencia;
	public float municion;
	public GameObject controlador;
	public float vida;
	public GameObject particles;
	public GameObject aletaDerecha;
	public GameObject aletaIzquierda;
	public GameObject aletaTraseraVertical;
	public GameObject aletaTraseraHorizontal;

	public float velocidadPaleta;

	void Start () 
	{
		repararCadencia = true;
	}
	
	void Update () 
	{
		ControlAvion();
		
		ControlVelocidad();
		//MOVIMIENTO AVION, SE MUEVE HACIA FU FORWARD (HACIA DELANTE)
		transform.Translate(transform.forward * velocidadMov * Time.deltaTime,Space.World);

		ControlHelice();
		
		if(Input.GetKey(KeyCode.Space) && !estadisparando && repararCadencia == true && municion > 0)
		{
			if(changeTorreta == true)
			{
				DisparosAvion2();
				repararCadencia = false;
			}
			else
			{
				DisparosAvion1();
				repararCadencia = false;
			}
		}
		repararCadencia = true;
		if(vida <= 0)
		{
			GameObject explosion = Instantiate(particles, transform.position, Quaternion.identity);
			Destroy(explosion,3f);
			Destroy(this.gameObject);
			
		}
	}

	private void ControlAvion()
	{
		//CONTROL MOVIMIENTO AVION
		if (Input.GetKey(KeyCode.W))
		{
			transform.Rotate(transform.right * velocidadGiro * Time.deltaTime,Space.World);
		}

		if (Input.GetKey(KeyCode.S))
		{
			transform.Rotate(-transform.right * velocidadGiro * Time.deltaTime,Space.World);
		}

		if (Input.GetKey(KeyCode.A))
		{
			transform.Rotate(-transform.up * velocidadGiro * Time.deltaTime,Space.World);
		}

		if (Input.GetKey(KeyCode.D))
		{
			transform.Rotate(transform.up * velocidadGiro * Time.deltaTime,Space.World);
		}

		if (Input.GetKey(KeyCode.Q))
		{
			transform.Rotate(transform.forward * velocidadGiro * Time.deltaTime,Space.World);
		}
	
		if (Input.GetKey(KeyCode.E))
		{
			transform.Rotate(-transform.forward * velocidadGiro * Time.deltaTime,Space.World);
		}
		
	}

	private void ControlVelocidad()
	{
		//CONTROLAR LA VELOCIDAD DEL AVION CON TOPES
		if(Input.GetKey(KeyCode.LeftShift))
		{
			velocidadMov += aceleracionMov;

			if(velocidadMov >= velMax)
			{
				velocidadMov = velMax;
			}
		}
		if(Input.GetKey(KeyCode.LeftControl))
		{
			velocidadMov -= aceleracionMov;

			if(velocidadMov < velMin)
			{
				velocidadMov = velMin;
			}
		}
	}

	private void ControlHelice()
	{
		//GIRAMOS LAS HELICES DEPENDIENDO DE LA VELOCIDAD DE MOVIMIENTO DEL AVION
		elice.transform.Rotate(0, velocidadMov * 2, 0);
	}
	private void DisparosAvion1()
	{
		//DISPARO DE UNO DE LOS CAÑONES
		GameObject proyectil = Instantiate(bala, salidaCanon1.transform.position, Quaternion.identity );
		proyectil.GetComponent<Rigidbody>().AddForce(salidaCanon1.transform.forward * velocidadBala , ForceMode.Impulse);
		gameObject.GetComponent<AudioSource>().Play(0);
		StartCoroutine(recargaAvion());
		changeTorreta = true;
		municion -= 1;
	}
	private void DisparosAvion2()
	{
		//DISPARO DE UNO DE LOS CAÑONES
		GameObject proyectil = Instantiate(bala, salidaCanon2.transform.position, Quaternion.identity );
		proyectil.GetComponent<Rigidbody>().AddForce(salidaCanon2.transform.forward * velocidadBala , ForceMode.Impulse);
		gameObject.GetComponent<AudioSource>().Play(0);	
		changeTorreta = false;
		municion -= 1;
		StartCoroutine(recargaAvion());
	}

	void OnCollisionEnter(Collision collision)
  {
		if(collision.gameObject.tag == "enemyMun")
		{
			vida --;
		}
		if(collision.gameObject.tag == "municionCaja")
		{
			municion += 50;
			if (municion > 100)
			{
				municion = 100;
			}
			controlador.GetComponent<manager>().spawnCaja = false;
			controlador.GetComponent<manager>().randomSpanCaja = false;

		}
		if(collision.gameObject.tag == "enemy")
		{
			vida = 0;
		}
	
}
	IEnumerator recargaAvion()
	{
		estadisparando = true;
		yield return new WaitForSeconds (tiempoCadencia);

		estadisparando = false;
		yield return 0;
	}

}
