﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class munitionText : MonoBehaviour {

private Text municionText;
private GameObject player;

	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("Player");
		municionText = GetComponent<Text>();		
	}
	
	void Update () 
	{
		municionText.text = player.GetComponent<ControladorAvion>().municion.ToString("00") ;
	}
}
