Pass:

+ ESTOS PUNTOS CORRESPONDEN AL PASS DE LA ANTERIOR ENTREGA

1. MODELO AVIoN.
Creacion de un avion usando las primitivas que ofrece Unity.
Posibilidad de utilizar un modelo externo o propio para el avion.

2. CONTROL DEL AVIÓN.
Implementar controles basicos de un avion. Movimiento WASD y	control del velocidad, acelerar y frenar. 
Control de la velocidad maxima y la velocidad minima de vuelo.
Incluir al modelo grafico del avion una helice que rote dependiendo de la velocidad del avion.

3. CONTROL DE CAMARA.
Implementacion del movimiento de la camara. La camara debera seguir el movimiento del avion y su 
rotacion, todo controlado desde un Script.
No vale hacer a la camara hijo del Avion.

4. TORRETA ENEMIGA.
Crear un modelo grafico para la torre (primitivas, externo o modelado).

2 rangos de vision:
- 1. La torre apunta al avion, preparandose para el disparo.
- 2. La torre dispara al avion misiles con un tiempo de cadencia. 
Tiempos de espera entre los disparos de los misiles.

Crear un modelo grafico para los misiles (primitivas, externo o modelado). Los misiles se autodestruyen 
pasados unos segundos. Al menos que le den tiempo a llegar al avion.


5. SISTEMA DE DISPAROS.
Incluir un sistema de disparo para el avion. Tecla para disparar y cadencia de disparo entre misiles. Los 
misiles del avion NO tendran que ser guiados, su direccion dependera del apuntado del jugador.


6. SISTEMA DE CONTROL DE DAÑOS:
- Avion y Escenario.
- Avion y Misiles Enemigos.
- Torretas y Misiles Avion.


+ ESTOS PUNTOS CORRESPONDEN AL MERIT DE LA ANTERIOR ENTREGA PERO EN ESTA SON NECESARIOS PARA EL PASS

- Mejorar el punto de salida de los misiles de la torre y el avion.
Ajustar la salida de los misiles para que cuadren con el modelo grafico.

- Implementar un sistema de municion para el avion. Recogeremos cajas flotantes para recuperar municion 
que se gastara al disparar:
- Las municiones solo puede ser recogidas por el jugador.
- La municion tiene una cantidad maxima que no se puede sobrepasar.
- Gestionar el tiempo de cadencia de disparo ( tiempo que pasa entre disparos) con CORUTINAS.

- Incluir sonidos en tu juego. Gestionalos eficientemente.


+ ESTOS PUNTOS CORRESPONDEN A LOS NUEVOS REQUISITOS DEL PASS DE ESTA NUEVA ENTREGA

- Implementar sistemas de particulas para crear explosiones que se crearon al impactar los misiles (escenario, avion o enemigos) y la destruccion del avion.

- Crear escenas adicionales que se presente en el momento adecuado:
- Pantalla de GameOver al morir (botones al Menu y Reintentar).
- Pantalla de Victoria al eliminar a todos los enemigos (botones al Menu y Reintentar).
- Pantalla de Menu. Pantalla inicial con el nombre y apellidos del autor.



Merit:

+ ESTOS PUNTOS CORRESPONDEN AL MERIT DE LA ANTERIOR ENTREGA

- Modificar el comportamiento de los misiles de la Torre para que sean misiles guiados y persigan al avion 
hasta su destruccion (pasandos unos segundos). Ajustar los misiles para que sean esquivables por el 
jugador.	

- Incluye un control libre de la camara controlado por el movimiento del raton al pulsar una tecla (a tu 
eleccion). Al dejar de pulsar dicha tecla la camara volvera a su comportamiento por defecto.


+ ESTOS PUNTOS CORRESPONDEN A LOS NUEVOS REQUISITOS DEL MERIT DE ESTA NUEVA ENTREGA

- Implementacion de diversos elementos HUD del juego asi como si gestion en la escena.
- Contador de enemigos restantes. Condicion de victoria.
- Contador de municion restante.

- Guardar informacion del juego en el disco. El jugador puede introducir su nombre en el Menu y ver su nombre durante el resto del juego (UI).

- Utiliza un GameManager para gestionar la escena y controlar el estado de jueg ( condiciones de Victoria y Derrota ). Utiliza el Patron Singleton.


Distinction:

+ ESTOS PUNTOS CORRESPONDEN AL DISTINCTION DE LA ANTERIOR ENTREGA

- CREAR UNA NUEVA TORRETA ENEMIGA

- Cambiar la logica de lanzamientos de misiles. La torreta dispondra de 4 misiles, para lanzar 
rapidamente (tiempo de cadencia pequeño 2-3 segundos). Una vez lanzados esos 4 misiles, tendra un 
tiempo de recarga mayor para disparar la siguiente serie de misiles.

- Utilizar Tablas/Arrays para la implementacion de este comportamiento. Los misiles estan 
diferenciados entre si ( 0 , 1 , 2 y 3 ) y se disparan siempre en el mismo orden.

- Diferenciar a este tipo de torretas con otro modelo grafico. Valdria simplemente cambiar el material del 
modelo grafico y su color.


- Incluye una limitacion a la rotacion del cañon de la torreta enemiga tanto superior como inferior. De esta 
manera crearemos ciertos angulos muertos que el jugador puede aprovechar donde la torreta no podra
apuntarle facilmente.

- Crea un Gameplay. Crea un objetivo para tu juego, ya sea superar ciertos obstaculos, eliminar a un n�mero 
de enemigos, sobrevivir un tiempo determinado, ect.



+ ESTOS PUNTOS CORRESPONDEN A LOS NUEVOS REQUISITOS DEL DISTINCTION DE ESTA NUEVA ENTREGA

- Guardando informacion en disco y mostrandola al jugador, implementa un sistema de puntuacion, tiempo que tardamos en eliminar a los enemigos.

- Contador de tiempo desde el inicio de la partida.

- Almacenar esta informacion cuando se produzcan las condiciones de victoria.

- Mostrar en el HUD el tiempo que llevamos actualmente en tiempo real.

- Sistema Ranking. Almacena los 3 mejores registros y muestralos en un men� de manera correcta ( nombre del jugador, orden, registros,...)